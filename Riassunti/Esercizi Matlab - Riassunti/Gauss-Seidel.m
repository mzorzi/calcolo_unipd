function [x,flag,nIter] = GaussSeidel(A,b,x0,tol,maxIter)
% Rivisto by Caesar =)

%Inizializzo le variabili
x = x0; %vettore iniziale
flag = 0;
nIter = 0;

%Matrici M ed N secondo GaussSeidel - Mx=Nx+b
M_gs = tril(A); % M=D-E
N_gs = -triu(A,+1); % N=F (quando uso tril/triu il numero che metto dopo la virgola � il riferimento alla diagonale, per upper + verso su, per lover � l'opposto)

%Calcolo il residuo
residuo = norm(b-A*x);

while residuo > tol && nIter<maxIter,
	nIter = nIter + 1;
	x = M_gs\(N_gs*x+b);% risolvo il sistema Mx=Nx+b
	residuo = norm(b-A*x);
end

if (residuo < tol)
	flag = 1;
end;

% by Caesar