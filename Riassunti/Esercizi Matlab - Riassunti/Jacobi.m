function [x,flag] = Jacobi(A,b,x0,tol,kmax)

% Vettore iniziale
x = x0;
flag = 0;
k = 0;

% Matrici di Jacobi
M_j = diag(diag(A));    % M=D
N_j = -(tril(A,-1)+triu(A,+1));    % N=B+C

normResiduoIni = norm(b-A*x);    % norma del residuo iniziale
err = normResiduoIni;    % una prima stima dell'errore

% Ciclo iterativo principale
while (err >= tol && k<kmax)
    k = k+1;
    x = M_j\(N_j*x+b);    % Risolve Mx=Nx+b con \
    residuo = b-A*x;    % calcola il residuo
    err = norm(residuo)/normResiduoIni;    % stima dell'errore relativo
end

if (residuo<tol)
    flag = 1;    % flag=1 metodo convergente
end

% by Caesar