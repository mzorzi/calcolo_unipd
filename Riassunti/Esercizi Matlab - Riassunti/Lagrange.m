function yL = Lagrange(P,x,n)
% alla fine ho un valore, che devo assegnare ad una variabile.
% versione rivista by Caesar

% Dati in input
%	P:		matrice contente le ascisse e le ordinare dei punti da interpolare es: P = (x0,y0;x1,y1)
%	x:		ascissa in cui si vuola valutare il polinomio di interpolazione
%	n:		grado del polinomio di interpolazione - al massimo pari al numero di punti-1
%
% Dati in output
%	yL:		valore della funzione

%calcolo i polinomi elementari di langrange sino all'ordine di input
for k=1:n+1
	L(k)=1;
	for j=1:k-1
		L(k) = L(k) * (x-P(j,1))/(P(k,1)-P(j,1)); % i valori delle ascisse dei punti (x) le ho sulla prima colonna
	end
	for j= k+1:n+1
		L(k) = L(k) * (x-P(j,1))/(P(k,1)-P(j,1));
	end
end

%calcolo la y secondo il polinomio
yL=0;
for k=1:n+1
	yL = yL + P(k,2)*L(k);
end

% by Caesar