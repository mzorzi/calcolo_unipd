function [x, nIter] = Newton(x0,maxIter,toll,f,df)
% f (e df) deve essere definita mediante il comando f = inline('espressione')

% Dati in input
%	f:			funzione
%	df:			derivata della funzione
%	maxIter:	massimo numero di iterazioni permesse
%	x0:			valore iniziale
%
% Dati in output
%	x:			0 della funzione
%	nIter:		numero di iterazioni effettuate

x = x0;
nIter = 0;
diff = toll + 1; % cos� sicuramente il while fa almeno un giro
while abs(diff)>toll*abs(x) && nIter<=maxIter, %essendo x1=x0-f(x0)/f'(x0) ==> x1-x0=-f(x0)/f'(x0)=diff
	nIter = nIter + 1;
	diff = -feval(f,x)/feval(df,x);
	x = x + diff;
end

if nIter >= maxIter
	disp('Superate le iterazioni massime');
else
	disp([x]);
	disp([nIter]);
end

% by Caesar