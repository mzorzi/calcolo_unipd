% Fattorizzazione LU rivista by Cesare:

A1 = [4 2 0.1 1.1; 1.2 5 3 0.3; 1.1 0.8 4 2.1; 0.3 1.2 3 5];
b = [1; 1; 1; 1];
A1, b, disp('A e b iniziali (premi un tasto per continuare)'), pause

n = size(A1,1); 
M=eye(n); %Creo una matrice Identit� di ordine n a cui aggiunger� i vari m
for k=1:n-1  
  for i=k+1:n
    M(i,k) = A1(i,k)/A1(k,k);  % elemento pivotale che scrivo gi� nella futura matrice L
    for j=k:n 
      A1(i,j) = A1(i,j) - M(i,k)*A1(k,j); 
    end
  end
  %MEG: b(i) = b(i) - M(i,k)*b(k);
  M
  A1, disp('(premi un tasto per continuare)'), pause
end 

U = A1;  
U
disp('U � il fattore triangolare superiore'), pause

L = M;
L
disp('L � il fattore triangolare inferiore (notare che ha tutti "1" sulla diagonale)'), pause

A3=L*U;
A3
disp('verifica: L*U=A '), pause

% soluzione in avanti del sistema L*y=b
y(1) = b(1)/L(1,1); 
for i=2:n 
	somma = 0;
	for j= 1:i-1
		somma = somma + L(i,j)*y(j);
	end
  y(i) = ( b(i) - somma ) / L(i,i); 
end; 

% soluzione all'indietro del sistema U*x=y
x(n) = y(n)/U(n,n);
for i=n-1:-1:1
	somma = 0;
	for j=i+1:n
	 somma = somma + U(i,j)*x(j);
	end
    x(i) = ( y(i) - somma ) / U(i,i);
end;
x
(A3*x')-b  % verifica
% MEG
% Nel caso di solo MEG avrei finito prima, dopo aver ottenuto la nuova A avrei risolto all'indietro % metodo delle sostituzioni all'indietro il sistema Ax=b

% by Caesar