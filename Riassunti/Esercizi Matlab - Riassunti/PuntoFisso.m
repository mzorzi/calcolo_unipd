function [x1, nIter] = PuntoFisso(x0,maxIter,toll,g)
% g deve essere definita mediante il comando g = inline('espressione')

x1 = g(x0);
nIter = 1; % ho gi� fatto un primo giro!
while abs(x1-x0)>toll*abs(x1) && nIter<=maxIter,
	x0 = x1;
	x1 = feval(g,x0);
	nIter = nIter + 1;
end
% N.B.: quando arrivo alla soluzione (alfa) allora, per propriet� del punto
% fisso g(alfa) = alfa! quindi x0 ed x1 sono il medesimo numero, quindi ho
% raggiunto la soluzioni una iterazione prima!
if nIter >= maxIter
	disp('Superate le iterazioni massime');
else
	disp([x1]);
	nIter = nIter - 1;
	disp([nIter]);
end	

% by Caesar