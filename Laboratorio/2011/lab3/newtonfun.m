function [xv,fxv,nit]=newtonfun(x0,nmax,toll,f,df)

%NEWTONFUN Metodo di Newton
%
% [xv,fxv,nit] = newtonfun (x0,nmax,toll,f,df)
%
% Dati di ingresso:
%   f:      funzione 
%   df:     derivata prima 
%   x0:     valore iniziale
%   toll:   tolleranza richiesta per l'ampiezza
%           dell'intervallo
%   nmax:   massimo numero di iterazioni permesse
%
% Dati di uscita:
%   xv:      vettore contenente le iterate 
%   fxv:     vettore dei residui
%   nit:     numero di iterazoni effettuate


x=x0;nit=0;
diff=toll+1;
xv = [];
fxv=[];
while abs(diff)>toll & nit<=nmax  
  nit=nit+1;
  diff= -feval(f,x)/feval(df,x);
  x=x+diff;
  fx=feval(f,x);
  xv = [xv;x];
  fxv=[fxv;fx];
end
if nit<=nmax  
  disp(['Convergenza ad iterazione k: ' num2str(nit),'  radice: ',num2str(x)]);
else
    disp(['E` stato raggiunto il numero massimo di passi']);
end

