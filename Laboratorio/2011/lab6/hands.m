% Fare il grafico della propria mano.
% Disegnare su un foglio il contorno della propria mano;
% Dare i seguenti comandi:

figure('position',get(0,'screensize'));
axes('position',[0 0 1 1]);
[x,y]=ginput;

% Cliccare su una serie di punti sul contorno della mano, alla fine
% dare INVIO;

% I vettori x e y sono i valori di due funzioni di una variabile
% indipendente t. Interpolare entrambe le funzioni mediante le spline:

n=length(x);
s=(1:n)';
t=(1:0.05:n)';

for spline_type=1:3

switch spline_type
case 1
    u=interp1(s,x,t,'nearest');
    v=interp1(s,y,t,'nearest');
case 2
    u=interp1(s,x,t,'linear');
    v=interp1(s,y,t,'linear');
case 3
    u=interp1(s,x,t,'spline');
    v=interp1(s,y,t,'spline');
end

clf reset;

plot(x,y,'o',u,v,'-r','LineWidth',2); hold on;
fprintf('\n \t [PAUSE]');
pause

format long
[x y]

end

