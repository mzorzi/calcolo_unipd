function ys = lagrange(x, y, xs, n)   
%LAGRANGE Formula di Lagrange per interpolazione
%
% ys = lagrange (x, y, xs, n) 
%
% Dati di ingresso:
%   x:  Vettore contenente i nodi di interpolazione
%   y:  Vettore contenenti le ordinate dei dati da 
%       interpolare nei nodi x
%   xs: Ascissa in cui si vuole valutare il polinomio 
%       di interpolazione
%   n:  grado del polinomio di interpolazione
%
% Dati di uscita:
%   ys: valore di P_n in xs, approssimazione di f(xs)

%Inizializzazione del vettore L
L=ones(n+1,1);

%  Calcolo dei polinomi caratteristici di Lagrange in xs
for i = 1:n+1;
   L(i)=1;
   for j = 1:i-1;
      L(i)=L(i) * (xs-x(j))/(x(i)-x(j));    
   end;  
   for j = i+1:n+1;
      L(i)=L(i) * (xs-x(j))/(x(i)-x(j));     
   end;    
end;    

%  Calcolo di ys = P_n(xs) con la formula di Lagrange  
ys = 0;
for  i = 1:n+1;
   ys = ys + y(i)*L(i);
end;

