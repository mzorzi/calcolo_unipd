f = inline('exp(x)./(x.^5)');
a=3;b=15;               % Estremi intervallo
n = 5;                  % Grado del polinomio di interpolazione
m =50;                  % Numero di punti di valutazione del polinomio
x = linspace(a,b,n+1);  % Punti di definizione del polinomio
y = f(x);               % funzione nei punti xi
xint = linspace(a,b,m); % Punti su cui valutare il polinomio
fint = f(xint);         % Funzione nei punti xint

% Calcola il polinomio di Lagrange
for k = 1:m
   yint(k) = lagrange(x, y, xint(k), n);
end
% Calcola i valori della funzione errore nell'intervallo di interpolazione
err = abs(fint-yint);
err_max=max(err);
% Crea un'unica figura che contiene il polinomio e la funzione
figure(1);
plot(xint,yint,'r',xint,fint,'k','Linewidth',2);
hold on
plot(x,y,'ob','Linewidth',3)
legend('P_n(x)','f(x)','Location','NorthWest');
title('Funzione e polinomio nell''intervallo di interpolazione');
% Crea la figura che contiene la funzione errore
figure(2);
plot(xint,err,'b','Linewidth',2);
title('Funzione errore nell''intervallo di interpolazione');
disp('Errore in norma infinito')
err_max