
% La routine di interpolazione polinomiale a tratti di Matlab si chiama interp1. 
% La sua sintassi � la seguente:
% yy = interp1( x, fx, xi, tipo), dove
% x � la griglia di interpolazione;
% fx sono i valori da interpolare;
% xi sono i punti sui quali si vuole calcolare l�interpolante
% 'tipo' � il tipo di interpolazione richiesta:
% - �nearest� : interpolazione con polinomio costante a tratti
% - �linear� : (default) polinomio lineare a tratti
% - �cubic� : polinomio cubico a tratti (con derivate continue)
% - �spline� : interpolazione con spline cubica




clear all, close all
f=inline('exp(x).*cos(4*x)');
x=0:3/10:3;
fx=f(x);
xx=0:0.01:3;
fxx=f(xx);




inear=interp1(x,fx,xx,'nearest');
ilin=interp1(x,fx,xx,'linear');
icub=interp1(x,fx,xx,'cubic');
ispl=interp1(x,fx,xx,'spline');
ispline=spline(x,fx,xx);



sbplt=1;

switch sbplt
    case 1       
    subplot(2,2,1);
    plot(xx,fxx,'Linewidth',2); hold on; plot(xx,inear,'m','Linewidth',2); plot(x,fx,'r*','Linewidth',2); title('int. costante a tratti');
    subplot(2,2,2);
    plot(xx,fxx,'Linewidth',2); hold on; plot(xx,ilin,'m','Linewidth',2); plot(x,fx,'r*','Linewidth',2); title('int. lineare a tratti');
    subplot(2,2,3);
    plot(xx,fxx); hold on; plot(xx,icub,'m','Linewidth',2); plot(x,fx,'r*','Linewidth',2); title('int. cubica a tratti');
    subplot(2,2,4);
    plot(xx,fxx,'Linewidth',2); hold on; plot(xx,ispl,'m','Linewidth',2); plot(x,fx,'r*','Linewidth',2); title('int. spline cubica');

    
    case 2
        figure(1)
        plot(xx,fxx,'Linewidth',2); hold on; 
        plot(xx,inear,'m','Linewidth',2); 
        plot(x,fx,'r*','Linewidth',2); title('int. costante a tratti');  hold off;
        figure(2)
        plot(xx,fxx,'Linewidth',2); hold on; 
        plot(xx,ilin,'m','Linewidth',2); 
        plot(x,fx,'r*','Linewidth',2); title('int. lineare a tratti'); hold off;
        figure(3)
        plot(xx,fxx,'Linewidth',2); hold on; 
        plot(xx,icub,'m','Linewidth',2); 
        plot(x,fx,'r*','Linewidth',2); title('int. cubica a tratti');  hold off;
        figure(4)
        plot(xx,fxx,'Linewidth',2); hold on; 
        plot(xx,ispl,'m','Linewidth',2); 
        plot(x,fx,'r*','Linewidth',2); title('int. spline cubica'); hold off;
        figure(5)
        plot(xx,fxx,'Linewidth',2); hold on; plot(xx,ispline,'m','Linewidth',2); plot(x,fx,'r*','Linewidth',2); title('spline cubica');
end
        











