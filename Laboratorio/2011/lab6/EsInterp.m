% IMPOSTAZIONE DATI DI INTERPOLAZIONE
% Vettori x e y contenenti i nodi di interpolazione e le ordinate dei dati 
% da interpolare, n grado del polinomio di interpolazione
 x=[1200.5 1201.5 1202.5 1203 1204 1205];
 y=[3 1.5 1.5 1 1 0];
 n=length(x)-1;
% Numero di punti di valutazione del polinomio np e coordinate dei punti di
% valutazione xp
 np=100;
 xp=linspace(x(1),x(end),np)';
 n=length(x)-1;

% METODO COEFFICIENTI INDETERMINATI
% Calcolo della matrice di Vandermonde
 V = vander(x);
% calcolo dei coefficienti come soluzione del sistema V a = y
 a = V\y';
% Valutazione del polinomio di coefficienti a nei punti del vettore xp
 pcind=polyval(a,xp);

% INTERPOLAZIONE DI LAGRANGE
for i=1:length(xp)
 plagr(i)=lagrange(x,y,xp(i),n);
end

% INTERPOLAZIONE CON POLYFIT
p=polyfit(x,y,n);
pfit=polyval(p,xp);

% VISUALIZZAZIONE RISULTATI
plot(xp,pcind,'m:',xp,plagr,'g-',xp,pfit,'r-.','Linewidth',2)
hold on
plot(x,y,'o','Linewidth',2)
legend('coefficienti ind.','Lagrange','polyfit', 'dati');



