close all, clear all

% Funzione di Runge
f=inline('1./(1+x.^2)');



% interpolazione su nodi equispaziati:
for i=2:2:30
  figure(1); xp=-5:0.05:5; plot(xp,f(xp),'r--','Linewidth',2); hold, 
             x=-5:10/i:5; c=polyfit(x,f(x),length(x)-1);  %Int. pol. grado 2,4,...30
             plot(xp,polyval(c,xp),'b-','Linewidth',2); hold,
             title('interpolazione su nodi equi-spaziati'),
             legend('Funzione','Pol.Interpolazione')
             pause;
end;



% Interpolazione con nodi di Chebychev:
a=-5; b=5;
for n=1:15
  figure(1); xp=-5:0.05:5; plot(xp,f(xp),'r--','Linewidth',2); hold, 
             x=(a+b)/2 - (b-a)/2*cos(pi*(0:n)/n);    %Nodi Chebychev
             c=polyfit(x,f(x),length(x)-1); 
             plot(xp,polyval(c,xp),'b-','Linewidth',2); hold,
             title('interpolazione agli zeri di Chebychev'), pause;
end;




% interpolazione lineare composita:
for i=2:2:30
  figure(1);  x=-5:0.05:5; plot(x,f(x),'r--','Linewidth',2); hold, 
              x=-5:10/i:5; xi=-5:0.05:5; yi=interp1(x,f(x),xi,'linear');
              plot(xi,yi,'b-','Linewidth',2); hold,
             title('interpolazione lineare composita'); pause,
end;



% interpolazione con splines cubiche interpolatorie:
for i=2:2:30
  figure(1); x=-5:0.05:5; plot(x,f(x),'r--','Linewidth',2); hold,
             x=-5:10/i:5; xi=-5:0.05:5; yi=interp1(x,f(x),xi,'spline');
             plot(xi,yi,'b-','Linewidth',2); hold,
             title('interpolazione con spline cubica'); pause,
end;
return

