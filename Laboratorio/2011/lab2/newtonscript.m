% Metodo di Newton per la ricerca degli zeri della
% funzione f. Test d'arresto basato sul controllo
% della differenza tra due iterate successive.


% Definizione funzione e sua derivata
f = inline('atan(7*(x-pi/2)) + sin((x-pi/2).^3)');
df =inline('7./(1+49*( x-pi/2 ).^2 )+3*(x-pi/2).^2 .*cos( (x-pi/2).^3 )');


% Inizializzazione e impostazione parametri
niter=0; nitermax=100;
toll=1e-8; x0=4;
x=x0;
diff=toll+1;

% Ciclo iterativo di convergenza
while abs(diff)>toll & niter<=nitermax  
  niter=niter+1;
  diff= -feval(f,x)/feval(df,x);
  xn=x+diff;
  disp(['k=' num2str(niter),'  x_k= ',num2str(x,10),' x_k-x_k-1= ',num2str(diff)]);
  x=xn;
end

% Output soluzione
if niter<=nitermax  
  zero=x;
  disp(['zero di f � x=' num2str(x,16)]);
else
  disp(['E` stato raggiunto il numero massimo di passi']);
end

