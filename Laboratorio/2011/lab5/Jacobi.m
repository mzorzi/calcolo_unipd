function [x,flag,res,iter] = Jacobi(A,b,x0,tol,maxiter)

% Vettore iniziale
x = x0;
flag = 0;
iter = 0;

% Matrici di Jacobi
M_j = diag(diag(A));               % M=D
N_j = -(tril(A,-1)+triu(A,+1));    %N=E+F

normb   = norm(b);      % norma del vettore b
residuo = norm(b-A*x);  % residuo iniziale

% salvo vettore residuo
res = residuo;

% Ciclo iterativo principale
while residuo >= tol && iter<maxiter,
    iter = iter+1;
    x = M_j\(N_j*x+b);        % Risolve Mx=Nx+b con \
    residuo = norm(b-A*x);    % calcola il residuo
    res(iter+1) = residuo;    % racoglie il residuo ad ogni iterazione
end

if( residuo<tol )
    flag = 1;                 % flag=1 metodo convergente
end
