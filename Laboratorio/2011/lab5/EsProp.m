n = 100;
UNIT = ones(n,1);
beta = 0.1;
A = (4+beta)*diag(UNIT) - diag(UNIT(1:n-1),-1) - diag(UNIT(1:n-1),1) ...
- diag(UNIT(1:n-2),-2) - diag(UNIT(1:n-2),2);

figure(1)
spy(A)

D = diag(diag(A));
E = -tril(A,-1); F = -triu(A,1);
Bj = inv(D) * (E+F); % matrice di iterazione di Jacobi
Bgs = inv(D-E) * F; % matrice di iterazione di Gauss-Seidel

rho_j = max(abs(eig(Bj)))
rho_gs = max(abs(eig(Bgs)))

x0 = zeros(n,1); b = ones(n,1); maxiter = 1000;tol = 1e-10;
[xj,flag,resj,iterj] = Jacobi(A,b,x0,tol,maxiter);
xj
flag
iterj

[xGs,flag,resGs,iterGs] = GaussSeidel(A,b,x0,tol,maxiter);
xGs
flag
iterGs
figure(2)
semilogy([0:iterj],resj,'b',[0:iterGs],resGs,'r','Linewidth',2)
legend('Jacobi','Gauss Seidel')
grid on
title('Metodi iterativi per beta = -0.1')