
 %A = [3  0  4; 7  4  2; -1 1  2]
% A = [-3 3  -6 ; -4  7 -8; 5 7 -9]
% A = [4  1 1; 2  -9  0; 0 -8 6]
A = [7 6  9; 4 5 -4; -7 -3 8]

D = diag(diag(A));      % D diagonale estratta da A			
E = -tril(A,-1);        % E matrice tr. inferiore di A
F = -triu(A,1);         % F matrice tr. superiore di A
PJ = inv(D) * (E+F);    % matrice di iterazione di Jacobi
PGs = inv(D-E)*F        % matrice di iterazione di GS

rho_j = max(abs(eig(PJ)))
rho_gs = max(abs(eig(PGs)))


b=[1 1 1]'; x0=b; tol=1e-10; maxiter=100;
[x1,flag1,res1,iter1] = Jacobi(A,b,x0,tol,maxiter);
x1, flag1,iter1
[x2,flag2,res2,iter2] = GaussSeidel(A,b,x0,tol,maxiter);
x2,flag2,iter2
semilogy([0:iter1],res1,'b',[0:iter2],res2,'r','Linewidth',2)
legend('Jacobi','Gauss Seidel')
grid on