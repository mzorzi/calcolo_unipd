 A = [4 2 0.1 1.1; 1.2 5 3 0.3; 1.1 0.8 4 2.1; 0.3 1.2 3 5];
 b = [1; 1; 1; 1];


A, b, disp('A e B iniziali (premi un tasto per continuare)'), pause

n = size(A,1); 
for k=1:n-1  
  for i=k+1:n
    m = A(i,k)/A(k,k);  % elemento pivotale
    for j=k:n 
      A(i,j) = A(i,j) - m*A(k,j); 
    end
    b(i) = b(i) - m*b(k);
  end
  A, b, disp('(premi un tasto per continuare)'), pause
end 

% metodo delle sostituzioni all'indietro
x(n,1) = b(n)/A(n,n);
for k=n-1:-1:1
  x(k,1) = ( b(k) - A(k,k+1:n)*x(k+1:n,1) ) / A(k,k);
end;
x

A*x-b  % verifica
norm(A*x-b)