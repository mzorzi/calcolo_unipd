
% Confronto soluzioni sistemi lineari

% Lo scopo dell'esercizio � confrontare i seguenti tre comandi MatLab per risolvere 
% un sistema lineare Ax = b:
%  x=A\b con A piena
%  x=inv(A)*b mediante calcolo dell'inversa con A in formato pieno
%  x=A\b con A in formato sparso

% Per avere una matrice signignicativa si cui provare l'esempio si utilizza il comando MatLab
% A = gallery('poisson',n) che genera una matrice sparsa di ordine n^2. 
% Per memorizzare una matrice in formato pieno si usa il comando full(A).


close all
clear all
clc


% CASO 1: sistema in formato pieno
ndims1 = [];
t1 = [];
err_rel1 = [];
for n=3:50
    disp(['n = ',num2str(n)]);
    A = full(gallery('poisson',n));
    xsol = ones(n^2,1);
    b = A*xsol;
    % Risoluzione del sistema lineare
    tic;
    x = A\b;
    t = toc; 
    % salvataggio informazioni
    ndims1(end+1) = n^2;
    t1(end+1) = t;
    err_rel1(end+1) = norm(x-xsol)/norm(xsol);
end


% CASO 2: sistema in formato pieno con calcolo dell'inversa
ndims2 = [];
t2 = [];
err_rel2 = [];
for n=3:45
    disp(['n = ',num2str(n)]);
    A = full(gallery('poisson',n));
    xsol = ones(n^2,1);
    b = A*xsol;
    % Risoluzione del sistema lineare
    tic;
    x = inv(A)*b;
    t = toc; 
    % salvataggio informazioni
    ndims2(end+1) = n^2;
    t2(end+1) = t;
    err_rel2(end+1) = norm(x-xsol)/norm(xsol);
end

% CASO 3: sistema in formato sparso
ndims3 = [];
t3 = [];
err_rel3 = [];
for n=3:200
    disp(['n = ',num2str(n)]);
    A = gallery('poisson',n);
    xsol = ones(n^2,1);
    b = A*xsol;
    % Risoluzione del sistema lineare
    tic;
    x = A\b;
    t = toc; 
    % salvataggio informazioni
    ndims3(end+1) = n^2;
    t3(end+1) = t;
    err_rel3(end+1) = norm(x-xsol)/norm(xsol);
end

% plot tempi di calcolo
h = plot(ndims1,t1,'b*-',ndims2,t2,'k*-',ndims3,t3,'r*-');
set(h,'LineWidth',2);
xlabel('Dimensione');
ylabel('s');
title('Tempi di calcolo');
legend('A full','Ainv','A sparse');


% plot accuratezza
figure;
h = plot(ndims1,err_rel1,'b*-',ndims2,err_rel2,'k*-',ndims3,err_rel3,'r*-');
set(h,'LineWidth',2);
xlabel('Dimensione');
ylabel('s');
title('Errore relativo');
legend('A full','Ainv','A sparse');


