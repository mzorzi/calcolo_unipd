function [xn,i,err,flag]=Jacobi(A,b,nmax,tol)
%------------------------------------------
% Metodo iterativo di Jacobi per sistemi lineari.
%-----------------------------------------
% Inputs:
% A : Matrice del sistema
% b : Termine noto (vettore riga)
% nmax : Numero massimo di iterazioni
% tol : Tolleranza sul test d'arresto (fra iterate)
%
% Outputs
% xn : Vettore soluzione
% i : Iterazioni effettuate
% err: vettore errori relativi in norma 2
% flag: se flag=1 converge altrimenti flag=0
%
%
% x0 : guess iniziale
%-------------------------------------------------------
flag=1;
D=diag(diag(A));
J=-inv(D)*(A-D);
q=inv(D)*b;
n=6;
x0=zeros(1,n)';
xn=J*x0+q;
i=1;
err(i)=norm(xn-x0)/norm(xn);
while (i<=nmax & err(i)>tol)
	x0=xn;
	xn=J*x0+q;
	i=i+1;
	err(i)=norm(xn-x0)/norm(xn);
end
if i>nmax,
	disp('** Jacobi non converge nel numero di iterazioni fissato');
	flag=0;
end
