function x = MEG (A,b)
% METODO DI ELIMINAZIONE DI GAUSS Ax=b
%  i 3 cicli eseguono la parte di eliminazione, poi per trovare
%  la soluzione si fa la sostituzione all'indietro
n=length(A);

for i=1:n-1,
    for j=i+1:n,
    m=A(j,i)/A(i,i);
        for k=i:n,
            A(j,k)=A(j,k)-m*A(i,k);
        end
    b(j)=b(j)-m*b(i);
    end
end
% A ora è triangolare superiore.
x = BS(A,b);
end

