function [sol,iter,err, flag]=GaussSeidel(A,b,kmax,tol)
% ----------------------------------------------
% Funzione che implementa il metodo iterativo
% di Gauss-Seidel
%-----------------------------------------------
% Inputs
% A, b: matrice e termine noto, rispettivamente
% 
% tol : tolleranza calcoli
%
% Outputs
% sol : vettore soluzione
% iter: numero delle iterazioni
% err: vettore errori relativi in norma infinito
% flag: booleano convergenza o meno
%
% x0 : guess iniziale
%-----------------------------------------------
x0=zeros(6,1);
n=length(x0); flag=1;
D=diag(diag(A));
L=tril(A)-D; U=triu(A)-D;
DI=inv(D+L); GS=-DI*U;
disp('raggio spettrale matrice iterazione di Gauss-Seidel');
max(abs(eig(GS))) 
b1=DI*b; 
x1=GS*x0+b1; 
k=1;
err(k)=norm(x1-x0,inf)/norm(x1);
while(err(k)>tol & k<=kmax)
	x0=x1;
	x1=GS*x0+b1;
	k=k+1;
	err(k)=norm(x1-x0,inf)/norm(x1);
end
if k>kmax,
	disp('** Gauss-Seidel non converge nel numero di iterazioni fissato');
	flag=0;
end
sol=x1; iter=k-1;
