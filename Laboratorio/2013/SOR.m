
function [xv,iter,err,est_omega,flag]=SOR(A,b,xin,nmax,toll)
%-------------------------------------------------------
% Parapmetri di ingresso:
% A : Matrice del sistema
% b : Termine noto (vettore riga)
% xin : Vettore iniziale (vettore riga)
% nmax : Numero massimo di iterazioni
% toll : Tolleranza sul test d’arresto (fra iterate)
%
% Parametri in uscita
% xv : Vettore soluzione
% iter : Iterazioni effettuate
% err : vettore degli errori
% est_omega: omega ottimale w0
% flag : se flag=1 converge altrimenti flag=0
%-----------------------------------------------------------

D=diag(diag(A)); B=-(tril(A)-D); C=-(triu(A)-D);
w=linspace(.1,1.9,100); 
for i=1:100
    p(i)=max(abs(eig(inv(D-w(i)*B)*((1-w(i))*D+w(i)*C))));
end;
plot(w,p);
xlabel(' \omega '); ylabel(' p(H_\omega) ');
title('Raggio spettrale della matrice del metodo SOR');
[mp,imp]=min(p);
est_omega=w(imp);

flag=1; [n,m]=size(A); D=diag(A); dm1=ones(n,1)./D; dm1=diag(dm1);
B=eye(size(A))-dm1*A; g=dm1*b; bu=triu(B); bl=tril(B);
%-------------
% Iterazioni
%------------
xv=xin; xn=xv;
i=0;
while i<nmax,
    for j=1:n;
        xn(j)=(1-est_omega)*xv(j)+est_omega*(bu(j,:)*xv+bl(j,:)*xn+g(j));
    end;
    if abs(xn-xv)<toll*abs(xn),
        iter=i;
        i=nmax+1;   
    else
        err(i+1)=[norm(xn-xv)/norm(xn)];
        xv=xn;
        i=i+1;
    end
end
if i==nmax,
    flag=0;
end
figure(2);
semilogy(err,'r-o');
title('Errore relarivo');
end