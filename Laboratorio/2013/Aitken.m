function [alfa,k]=Aitken(g,x0,tol,kmax)
%-----------------------------------------
% Metodo di accelerazione di Aitken
%------------------------------------------
% g e‘ la funzione di iterazione g(x)=x-f(x)
%-------------------------------------------
k=1; x1=g(x0); x2=g(x1);
xnew=x0-(x1-x0)^2/(x2-2*x1+x0);
e(k)= abs(x2-xnew);

while abs(x2-xnew)>tol && k <kmax
	x0=xnew;
	x1=g(x0);
	x2=g(x1);
	xnew=x0-(x1-x0)^2/(x2-2*x1+x0);
	k=k+1;
	e(k)=abs(x2-xnew);
end
% Al termine, alfa sara’ l’ultimo valore di xnew
alfa=xnew;
k=k-1;

semilogy(e);
axis tight;
grid;
return
