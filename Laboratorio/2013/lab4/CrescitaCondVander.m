clear all
close all

nmax=100;

for k=2:nmax
    c=linspace(-1,1,k);
    cnd(k)=cond(vander((c)));
    dd(k)=det(vander(c));
end

semilogy(cnd,'-o');
figure(2)

semilogy(dd,'-o');
