
clear all
close all
A= toeplitz([4,1,0,0,0,0]);
x=[2;2;2;2;2;2];
b=A*x;

solMEG = MEG(A,b);
%non è necessario fare pivoting poichè gli elementi sulla diagonale non
%sono nulli


% Thomas

[L,U]=Thomas(A);
z=L^-1*b;
solTh=U^-1*z;