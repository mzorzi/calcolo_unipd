function [L,U] = Thomas( A )
% Algoritmo di Thomas per matrici tridiagonali Ax=b
% A deve essere tridiagonale
% restituisce due matrici bidiagonali t.c. L*U=A
% per trovare la soluzione x del sistema basta risolvere il sistema
% Lz=b
% Ux=z

U(1,1) = A(1,1);
L(1,1) = 1;
n= size(A);
for i = 2 : n
    L(i,i-1) = A(i,i-1) / U(i-1,i-1);
    L(i,i) = 1;
    U(i,i) = A(i,i)- L(i,i-1) * A(i-1,i);
    U(i-1,i) = A(i-1,i);
end

end

