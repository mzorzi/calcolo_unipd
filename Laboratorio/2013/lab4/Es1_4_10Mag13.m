clear all
close all

nmax=4;

k=nmax;
    
    H=hilb(k);
    
    b=H*ones(k,1);
    
    db=[zeros(length(b)-1,1); 1.e-4]
    
    b1=b+db;
    
    s=H\b;
    s1=H\b1;
    
    Ex=norm(s-s1)/norm(s);
    Eb=norm(db)/norm(b);
    
    disp('stima numero condizionamento');
    cond_est=Ex/Eb
    
    disp('numero condizionamento')
    cond(H)
    
    disp('differenza')
    differenza=abs(cond_est-cond(H))