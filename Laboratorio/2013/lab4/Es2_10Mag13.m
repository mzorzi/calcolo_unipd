clear all
close all

m=100
for n=2:m
    
    x1=0:n;
    x2=0:1/n:1;
    x3=-0.5:1/n:0.5;
    
    c1(n-1)=cond(vander(x1));
    c2(n-1)=cond(vander(x2));
    c3(n-1)=cond(vander(x3));
end

semilogy(1:m-1,c1,'r-',1:m-1,c2,'g-',1:m-1,c3,'b-');
legend('Pti equispaziati','Pti scalati','Pti simmetrici');