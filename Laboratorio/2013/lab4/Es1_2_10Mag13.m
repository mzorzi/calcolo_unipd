clear all
close all

nmax=20;

for k=2:nmax
    
    H=hilb(k);
    
    b=H*ones(k,1);
    
    s=inv(H)*b;
    
    er(k)=norm(s-ones(k,1))
    
end

semilogy(er,'r-o');