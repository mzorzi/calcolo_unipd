clear all;
% script dell'esercizio 2, calcola la soluzione dei 4 sistemi
% lineari
A = [15,6,8,11;
     6,6,5,3;
     8,5,7,6;
     11,3,6,9;];
x=[1;1;1;1];

for i=1:4,
    Ai=A^i;
    b= Ai*x;
    c(i)=cond(Ai);
    xi=MEG(Ai,b);
    e(i)=norm(x-xi)/norm(x);
end

% l'errore aumenta, all'aumentare del condizionamento.
% loglog plotta in scala logaritmica sia ordinate che ascisse 
loglog(e,c,'-ro')