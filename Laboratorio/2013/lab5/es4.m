clear all
A = toeplitz([4,1,0,0,0,0]);
x=[2;2;2;2;2;2];

b=A*x;

[sol,iter,err,flag] = Jacobi(A,b,100,1.e-6);

semilogy(err,'-o');