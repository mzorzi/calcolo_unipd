clear all;

A = [8,1,2,0.5,2;
     1,0.5,0,0,0;
     2,0,4,0,0;
     0.5,0,0,7,0;
     2,0,0,0,16;];
x=[0;0;1;1;1];

b= A*x;

x1 = MEG(A,b);

% calcolo dell'errore, x1 soluzione con MEG, x soluzione 
% senza errore (data dal prof)
e=norm(x-x1)/norm(x)