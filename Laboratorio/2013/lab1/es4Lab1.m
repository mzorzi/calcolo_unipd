clear all

x = 1.005;

xx = linspace(1-2*1.e-8,1+2*1.e-8,401);

q7 = (xx-1).^7;

q71 = xx.^7-7*xx.^6+21*xx.^5 - 35*xx.^4 + 35*xx.^3 - 21*xx.^2 + 7*xx-1;

plot(xx,q7,'-r',xx,q71,'b-')