clear all
close all

nmax=200;
x=-20.5;
s(1)=0; n=1; t(1)=1;
v=exp(x);
for n=2:nmax
  t(n)=x^n/factorial(n);
  ss=0;
  for k=1:n-1
   ss=ss+x^(k-1)/factorial(k-1);
  end
  s(n)=ss+t(n-1);
  
  if (abs(t(n))/v < 1.e-16) 
     disp('error below 1.e-16') 
	 abs(t(n))/v, n
	 break;
  end
end

% seconda parte

  
clear all
close all

nmax=200;
x=20.5;
s(1)=0; n=1; t(1)=1;
v=exp(x);
for n=2:nmax
  t(n)=x^n/factorial(n);
  ss=0;
  for k=1:n-1
   ss=ss+x^(k-1)/factorial(k-1);
  end
  s(n)=ss+t(n-1);
  
  if (abs(t(n))/v < 1.e-16) 
     disp('error below 1.e-16') 
	 abs(t(n))/v, n
	 break;
  end
end

% terza parte

clear all
close all

nmax=200;
x=-1;
s(1)=0; n=1; t(1)=1;
v=exp(x);
tent=0;
for n=2:nmax
  t(n)=x^n/factorial(n);
  ss=0;
  for k=1:n-1
   ss=ss+x^(k-1)/factorial(k-1);
  end
  s(n)=ss+t(n-1);
  
  if (abs(t(n))/v < 1.e-16)  
	 tent = n;
	 break;
  end
end

nmax=200;
x=0.5;
s(1)=0; n=1; t(1)=1;
v=exp(x);
for n=2:nmax
  t(n)=x^n/factorial(n);
  ss=0;
  for k=1:n-1
   ss=ss+x^(k-1)/factorial(k-1);
  end
  s(n)=ss+t(n-1);
  
  if (abs(t(n))/v < 1.e-16) 
     disp('error below 1.e-16') 
	 abs(t(n))/v, tent+n
	 break;
  end
end
