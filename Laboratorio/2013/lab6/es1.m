
%fatto solo per jacobi, per gauss-seidel è uguale basta usare 
% l'iterazione di GS

clear all
close all

n=6;
A=diag(ones(n,1)*10)+diag(ones(n-1,1)*3,+1)+diag(ones(n-1,1)*3,-1);
b=(1:n)';
x0=zeros(1,n)';
tol = 1.e-9;

D=diag(diag(A));
J=-inv(D)*(A-D);
q=inv(D)*b;

x1=J*x0+q;


NJ = norm(J);
diff= norm(x1-x0);

var = tol*(1-NJ)/diff;

kmin = (log(var))/log(NJ)

%

[x,k] = Jacobi(A,b,100,tol);

k

