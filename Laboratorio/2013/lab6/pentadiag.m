function [ A ] = pentadiag(v,n)
%Funzione che crea una matrice pentadiagonale
% n = grandezza della matrice da creare
% v = vettore di 5 numeri da utilizzare per
%	creare le 5 diagonali

A=zeros(n,n);

for i=1:5
    AA=diag(ones(n,1),i-3);
    A=A+v(i)*AA(1:n,1:n);

end

