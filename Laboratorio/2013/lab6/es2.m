% è un esempio di chiamata alla funzione richiesta dall'esercizio
% la soluzione dell'esercizio è la funzione SOR.m
clear all
close all

n=10;
A=diag(ones(n,1)*10)+diag(ones(n-1,1)*3,+1)+diag(ones(n-1,1)*3,-1);
b=(1:n)';
x0= zeros(n,1);
tol = 1.e-9;
nmax = 100;

[sol,iter,err,omega,flag] = SOR(A,b,x0,nmax,tol);

b

A*sol