clear all
close all

%si usa la formula diretta poichè A tridiagonale 
%e gli autovalori di J reali t.c. il modulo del raggio spettarale di J minore di 1

n=10;
A=diag(ones(n,1)*10)+diag(ones(n-1,1)*3,+1)+diag(ones(n-1,1)*3,-1);
D=diag(diag(A));
J=-inv(D)*(A-D);

w0=2/(1+sqrt(1-max(eig(J))))