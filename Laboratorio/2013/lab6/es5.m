clear all
close all
n=10;
A=diag(1:n);

t=linspace(0,2/3,100);
m=length(t);

for k=1:m
    P=eye(n)-t(k)*A;
    
    r(k)= max(abs(eig(P)));
end

semilogy(t,r,'r.-',t,ones(m,1),'k');
[r_min,rk]=min(r);


%b
x=ones(n,1);

b=A*x;
x0=zeros(n,1);
kmax=100;
tol=1.e-6;

P= eye(n)-t(rk)*A;
q=t(rk)*b;
x1=P*x0+q;
k=1;
err(k)=norm(x1-x0)/norm(x1);


while(err(k)>tol & k<=kmax)
    x0=x1;
    x1=P*x0+q;
    k=k+1;
    err(k)=norm(x1-x0)/norm(x1);
    
end

x1
k-1
sol=inv(A)*b
figure(2)
semilogy(err,'b-o');
