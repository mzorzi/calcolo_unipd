function x = BS(U,b)
% Algoritmo di sostituzione all'indietro del metodo 
% di eliminazione di gauss.
% Ux=b
% U = Matrice che deve essere in forma triangolare superiore
n = length(U);
x = b;
x(n) = x(n)/U(n,n);
for i = n-1:-1:1
   x(i) = (x(i)-U(i,i+1:n)*x(i+1:n))/U(i,i);
end