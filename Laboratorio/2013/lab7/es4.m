
clear all
close all

    xbar=linspace(-1,1,2000)';


for n=2:20,
    x=linspace(-1,1,n); % equispaced interpolation points
    xcl=-cos([0:n-1]*pi/(n-1)); %chebsyshev-lobatto pts in  
    r=-1+rand(n,2);
    for k=1:length(x)
        L(k,:)=lagrange(k,x,xbar);
        L1(k,:)=lagrange(k,xcl,xbar);
        L2(k,:)=lagrange(k,r',xbar);
    end

    nl(n)=norm(L,1);
    nl1(n)=norm(L1,1);
    nl2(n)=norm(L2,1);
    
end

plot(nl,'b.-');
title('Lebesgue constant on equispaced points')

figure(2);
plot(nl1,'r.-');
title('Lebesgue constant on chebyshev points')

figure(3);
plot(nl2,'k.-');
title('Lebesgue constant on random points')

