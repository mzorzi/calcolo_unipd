
clear all
close all


xbar=linspace(-5,5,200)';
ybar=1./(1+xbar.^2);
plot(xbar,ybar,'g--');
grid;

for n=5:20,
    x=linspace(-5,5,n); % equispaced interpolation points
    xcl=-5*cos([0:n-1]*pi/(n-1)); %chebsyshev-lobatto pts in [-5,5]
    y=1./(1+x.^2);        %runge fct at the interp. pts
    ycl=1./(1+xcl.^2);  
    
    for k=1:length(x)
        L(:,k)=lagrange(k,x,xbar);
        L1(:,k)=lagrange(k,xcl,xbar);
    end
    
    p=L*y';
    pcl= L1*ycl';
    figure(1);
    plot(xbar,ybar,'g--',x,y,'bo',xbar,p,'r-');
    grid;
    
    figure(2);
    plot(xbar,ybar,'g--',xcl,ycl,'bo',xbar,pcl,'r-');
    grid;
    pause(2)
end
