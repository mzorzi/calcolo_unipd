clear all
close all

xbar=[ -5:0.1:5]';
ybar=erf(xbar);

for n=4:10, 
    x=linspace(-5,5,n);
    y=erf(x);
    c=polyfit(x,y,n-1);
    yy=polyval(c,xbar);
    
     plot(xbar,ybar,'g--',xbar,yy,'r-',x,y,'bo');
     
     pause(2);
end
