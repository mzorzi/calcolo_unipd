clear all
close all


xbar=linspace(-1,1,200)';
ybar=1./(1+(25*xbar).^2);
n=10;
x=linspace(-1,1,n); % equispaced interpolation points
xcl=-cos([0:n-1]*pi/(n-1)); %chebsyshev-lobatto pts
y=1./(1+25*x.^2);        %runge fct at the interp. pts
ycl=1./(1+25*xcl.^2);  
    
    for k=1:length(x)
        L(:,k)=lagrange(k,x,xbar);
        L1(:,k)=lagrange(k,xcl,xbar);
    end
    
p=L*y';
pcl= L1*ycl';
figure(1);
plot(xbar,ybar,'g--',x,y,'bo',xbar,p,'r-');
grid;
    
figure(2);
plot(xbar,ybar,'g--',xcl,ycl,'bo',xbar,pcl,'r-');
grid;