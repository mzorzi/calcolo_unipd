function [b]=DiffDivise(x,y)
%----------------------------------------------------------------------
% This function implements the algorithm of divided differences
%----------------------------------------------------------------------
% Inputs
% x: vector of interpolation points,
% y: vector of function values.
%
% Output
% b: vector of divided differences
%---------------------------------------------------
n=length(x); b=y; 
for i=2:n,
	for j=2:i,
		b(i)=(b(i)-b(j-1))/(x(i)-x(j-1));
	end;
end;
