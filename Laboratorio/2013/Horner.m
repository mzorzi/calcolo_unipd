
function p=Horner(d,x,xt)
%--------------------------------------------------------------------------------------------
% This function implements the Horner scheme for evaluating the interpolating polynomial
% in Newton form on a set of target points
%--------------------------------------------------------------------------------------------
% Inputs
% d: divided differences vector
% x: vector of interpolating points
% xt: vector of target points
%
% Output
% p: the polynomial evaluated at all targets
%-----------------------------------------------
n=length(d);
for i=1:length(xt);
	p(i)=d(n);
	for k=n-1:-1:1
		p(i)=p(i)*(xt(i)-x(k))+d(k);
	end
end
