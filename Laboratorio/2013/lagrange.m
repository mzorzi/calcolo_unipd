function l = lagrange(i,x,xbar)
%---------------------------------
% INPUTS
% i=index of the polynomial
% x=vector of interpolation points
% xbar= vector of target points
% (column vector!!)
%
% OUTPUT
% l=vector of the ith elementary
% Lagrange polynomial at xbar
%---------------------------------
n = length(x); m = length(xbar);
l = prod(repmat(xbar,1,n-1)-repmat(x([1:i-1,i+1:n]),m,1),2)/...
prod(x(i)-x([1:i-1,i+1:n]));