clear all
close all

a=1;
b=2;
tol= 1.e-6;
f= inline('20./(1+log(x.^2)) -5*sin(exp(x))');

A=[7/3,1.5,1;
   1,1,1;
   4,2,1];

B=[quadl(f,a,b,tol),f(1), f(2)]';

sol = MEG(A,B);
x= linspace(a,b,100);
y= f(x);
yy=polyval(sol,x);

plot(x,y,'b-',x,yy,'r-');
legend('f(x)','interpolating poly.')
display('errore')
norm(y-yy, inf)
