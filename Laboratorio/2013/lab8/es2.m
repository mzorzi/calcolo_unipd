clear all
close all

m=100;
a=-2;
b=2;
n=6;
f=inline('x + exp(x) + 20./(1 + x.^2) -5');
x = linspace(a,b,n);
y = f(x);

t=linspace(-2,2,m);
ty= f(t);

dd = DiffDivise(x,y);


p= Horner(dd,x,t);

plot(t,ty,'r-',t,p,'b-.',x,y,'ko');
legend('function', 'inter poly','interp points')

%part b

xstar= 1;
x=[x xstar];
y=[y f(xstar)];
dd= DiffDivise(x,y);

disp('errore in xstar')
errore= prod(xstar -x(1:end-1))*dd(end)


%part c


figure(2)

xche=2*(cos([1:2:2*n-1]*pi/(2*n)));
yche = f(xche);
ddche=DiffDivise(xche,yche);


pche= Horner(ddche,xche,t);
plot(t,ty,'r-',t,pche,'b-.',xche,yche,'ko');