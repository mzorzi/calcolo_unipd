clear all
close all
f=inline('x*exp(-x)*cos(2*x)');
% la derivata seconda è : e^(-x) (-(2+3 x) cos(2 x)+4 (-1+x) sin(2 x)) che
% il cui valore massimo in modulo in [0,2pi] è 3.00226

a= ((2*pi)^.3)/12*3.00226;
N=sqrt((1.e+6)*a); %659

% la derivata seconda è : e^(-x) ((44-7 x) cos(2 x)+8 (1-3 x) sin(2 x)) che
% il cui valore massimo in modulo in [0,2pi] è 12.2148

a= ((2*pi)^.5)/2880*12.2148;
N= ((1.e+6)*a)^(1/4); %11

n1=659;
h= 2*pi/n1;
int= f(0) + f(2*pi);
for i=1:n1-1,
    int= int+ 2*(f(h*i));
end
int= int*h/2
err= abs(-(0.12212260462) - int)/0.12212260462


n2=11;
h = (2*pi)/11;

sum_even = 0;

for i = 1:n2/2-1
   x(i) = a + 2*i*h;
   sum_even = sum_even + f(x(i));
end

sum_odd = 0;

for i = 1:n2/2
   x(i) = a + (2*i-1)*h;
   sum_odd = sum_odd + f(x(i));
end

integral = h*(f(0)+ 2*sum_even + 4*sum_odd +f(2*pi))/3
err2= abs(-(0.12212260462) - integral)/0.12212260462