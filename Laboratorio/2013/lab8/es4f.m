function [ y ] = es4f( x )

y=x/2.*exp(-x/2).*cos(x);

end

