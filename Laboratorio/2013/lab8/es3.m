clear all;
close all;

a=0; b= 2*pi;
 h=b/15;
 f=inline('sin(x)+ sin(5*x)');
for k=1:5,
    hi(k)= h/(2.^(k-1));
    x= a:hi(k):b;
    y= f(x);
    xx= a:hi(k)/10:b;
    ys=spline(x,y,xx);  %spline cubica
     
    %parte 2
    
    c= polyfit(x,y,8); %polinomio grado 8
    p= polyval(c,xx);
    
    draw = linspace(a,b,100);
    drawy = f(draw);
    
    plot(draw,drawy,'b-',xx,ys,'r-',xx,p,'g-');
    grid
    axis tight
    legend('function','spline','polyfit');
    
    pause(2);
end