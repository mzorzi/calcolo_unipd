clear all
close all

a=-1;
b=1;
f= inline('x/2.*exp(-x/2).*cos(x)');
disp('calcolo dell integrale con simpson')
((b-a)/6)*(f(a)+4*f((a+b)/2)+f(b))
disp('valore esatto')
quadl(f,-1,1)