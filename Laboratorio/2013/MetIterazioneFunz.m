function [x0, niter, flag]=MetIterazioneFunz(g,x0, tol, kmax)
%--------------------------------------
% Metodo d’iterazione funzionale
%--------------------------------------
% Inputs
% g: funzione di iterazione
% x0: guess iniziale
% tol: tolleranza
% kmax: numero massimo d’iterazioni
%
% Outputs
% x0: soluzione
% niter: iterazioni fatte
% flag: 1 o 0 per indicare se
% la convergenza c’e’ o meno
%----------------------------------------
x1=g(x0); k=1; flag=1;

e(k) = abs(x1-x0)/abs(x1);

while abs(x1-x0) > tol*abs(x1) && k <= kmax
	x0=x1;
	x1=g(x0);
	k=k+1;
	e(k) = abs(x1-x0)/abs(x1);
end
% Se converge, x0 oppure x1 contengono il valore
% dello zero cercato. Altrimenti, si pone flag=0
% che indica la non convergenza
if (k > kmax)
	flag=0;
end
niter=k-1;
semilogy(e);
axis tight;
grid;
return
