clear all
close all

f=inline('(9.81/(2*x.^2))*(sinh(x)-sin(x))-1');
g=inline('x-(9.81/(2*x.^2))*(sinh(x)-sin(x))-1');

[sol,k]=BISECTION(f,-2,1,1.e-6);

[x0,niter,flag]= MetIterazioneFunz(g,-2,1.e-6,50);