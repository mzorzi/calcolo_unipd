clear all
close all

f=inline('x.^3 - 8*x.^2 + 85/4*x - 75/4');
fplot(f,[-1,5])
grid
% dal plot della funzione si osserva che lo zero di modulo massimo si trova
% nell'intervallo [2.5,5]
figure(2)
[sol,iter,flag]= MetIterazioneFunz(@es5it,5,1.e-9,200);