clear all
close all

es1f =inline('x.^2-log(x.^2+2)');
fplot(es1f,[-5,5])
grid

figure(2)
es1g1 =inline('sqrt(log(x.^2+2))');
[x0,niter,flag]= MetIterazioneFunz(es1g1,1.5,1.e-6,50);

figure(3);

es1g2 =inline('x+x.^2-log(x.^2+2)');
 
[x0,niter,flag]= MetIterazioneFunz(es1g2,-1.5,1.e-6,50);

fzero(es1f,[0.5,1.5],optimset('Display','iter'))