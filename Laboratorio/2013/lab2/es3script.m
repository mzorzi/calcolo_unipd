clear all;

disp('prima radice utilizzando metodo di bisezione');
BISECTION(@es3f,-1,0,1.e-9);

disp('seconda radice utilizzando il metodo di newton');
[sol,iter,flag]= MetIterazioneFunz(@es3new,1,1.e-9,200)

disp('terza radice utilizzando il metodo di iterazione');
[sol,iter,flag]= MetIterazioneFunz(@es3g,4,1.e-9,200)

fplot(@es3f,[-1,5])
grid