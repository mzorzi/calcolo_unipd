clear all;

disp('prima radice utilizzando metodo di bisezione');
f=inline('exp(x)-4*(x.^2)');
BISECTION(f,-1,0,1.e-9);

new=inline('x-(exp(x)-4*(x.^2))/(exp(x)-8*x)');

disp('seconda radice utilizzando il metodo di newton');
[sol,iter,flag]= MetIterazioneFunz(new,1,1.e-9,200)

disp('terza radice utilizzando il metodo di iterazione');

g=inline('log(4*(x.^2))');
% la derivata della g è 2/x, per questo questo metodo può convergere solo
% alla radice nell'intervallo [4,4.5]

[sol,iter,flag]= MetIterazioneFunz(g,4,1.e-9,200)
grid