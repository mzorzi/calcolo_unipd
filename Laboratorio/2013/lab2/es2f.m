    function [ y ] = es2f( x )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
g=9.81;
y=(g/(2*x.^2))*(sinh(x)-sin(x))-1;

end

%attenti che con x=0 da NaN, poichè risulta 0*inf-1;
%quindi se usate il metodo di bisezione con intervallo [a,b]
%dove b=-a da problemi