clear all
close all

f=inline('(x - 1)*exp(x)');

% la g1 non converge perchè la sua derivata è 1 + 1/x, che per x positivo è
% sempre maggiore di 1
g1=inline('log(x*exp(x))');

% il modulo la derivata della g2 per x positivo è sempre minore di 1
g2= inline('(exp(x)+x)/(exp(x)+1)');
[sol,iter,flag]= MetIterazioneFunz(g2,2,1.e-10,100);

figure(2)

% la derivata della g3 è 1-1/x^2, quindi per avere convergenza bisogna
% prendere x0 > 1/sqrt(2)
g3=inline('(x.^2 - x +1)/x');
[sol2,iter2,flag2]= MetIterazioneFunz(g3,2,1.e-10,100);

figure(3)

% con il metodo di steffensen la funzione non converge per x0=2, mentre
% converge prendendo x0 più vicino alla radice, ad esempio per x0=1.5
[sol3,iter3,flag3]= MetIterazioneFunz(@es1steff,1.5,1.e-10,100);