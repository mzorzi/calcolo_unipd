function [sol,k]=BISECTION(fun,a,b,tol)
%------------------------------------
% Metodo di bisezione
% La funzione fun.m descrive la
% funzione di cui si cerca la radice
%------------------------------------
% Inputs
% a,b : estremi dell'intervallo
% tol : tolleranza
% Output
% sol : la soluzione cercata
% k : numero d'iterazioni fatte
%----------------------------------
if fun(a)*fun(b) > 0
    error('L''intervallo non contiene alcuna radice');
elseif abs(fun(a)*fun(b) ) < tol
error('Uno degli estremi e'' gia'' sulla radice ')
else
a0=a; b0=b; k=0;

x=ceil((log(abs(b0-a0))-log(tol))/log(2));
xstr = num2str(x);
iterprio=['Numero iterazioni da fare a priori : ',xstr];
disp(iterprio)
while abs(b-a)>tol*abs(b),
    m=(a+b)/2;
    if abs(fun(m))< tol
        mstr= num2str(m);
        rad = ['La radice cercata e` : ',mstr];
        disp(rad)
    break;
elseif fun(m)*fun(a)<0
b=m;
else
a=m;
end
k=k+1;
end
end
sol = (a+b)/2;
return
